package localdb;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;
import java.util.List;
import model.Record;

@Dao
public interface TaskDao {

    @Query("SELECT * FROM record")
    List<Record> getAll();

    @Insert
    void insert(List<Record> record);

    @Delete
    void delete(Record record);

    @Update
    void update(Record record);

}
