package localdb;

import androidx.room.Database;
import androidx.room.RoomDatabase;
import model.Record;

@Database(entities = {Record.class}, version = 1)
public abstract class AppDatabase extends RoomDatabase {
    public abstract TaskDao taskDao();
}
