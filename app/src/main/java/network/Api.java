package network;

import model.DriverListData;
import model.LoginData;
import model.User;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public interface Api {

    String BASE_URL = "http://whapidev.centralus.cloudapp.azure.com/CustomerStaging/api/";
    String LOGIN = BASE_URL + "Customerlogin";
    String DRIVER_LIST = BASE_URL + "MyDriverList?customer_id=6883757685";

    String IMAGE_BASE_URL = "https://whphotostore.blob.core.windows.net";

    @Headers({"Content-Type: application/json"})
    @POST(LOGIN)
    Call<LoginData> loginCall(@Body User user);

    @Headers({"Content-Type: application/json"})
    @GET(DRIVER_LIST)
    Call<DriverListData> getDriverList(@Header("Authorization") String auth);
}
