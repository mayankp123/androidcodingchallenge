package adapter;

import static network.Api.IMAGE_BASE_URL;
import android.app.Activity;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import com.bumptech.glide.Glide;
import com.example.androidcodingchallenge.R;
import java.util.List;
import butterknife.BindView;
import butterknife.ButterKnife;
import model.Record;

public class DriverListAdapter extends RecyclerView.Adapter<DriverListAdapter.ViewHolder>{

    public static final String TAG = "DriverListAdapter";

    ViewHolder viewHolder;
    Activity mActivity;
    private List<Record> list;
    private int lastPosition = -1;

    public DriverListAdapter(Activity mActivity, List<Record> driverList) {
        this.mActivity = mActivity;
        this.list = driverList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.driver_items, parent, false);
        viewHolder = new ViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        Record record = list.get(position);

        setDriverData(holder, record);

        setAnimation(viewHolder.itemView, position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    private void setDriverData(ViewHolder holder, Record record) {
        if(record!=null) {
            if(!TextUtils.isEmpty(record.getName())){
                holder.name.setText(record.getName());
            }
            if(!TextUtils.isEmpty(record.getWhideCode())){
                holder.white_code.setText(record.getWhideCode());
            }
            if(!TextUtils.isEmpty(record.getImageUrl())){
                Glide.with(mActivity).
                        load(IMAGE_BASE_URL+record.getImageUrl())
                        .fitCenter()
                        .centerCrop()
                        .into(holder.image);
            }
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.image)
        ImageView image;
        @BindView(R.id.name)
        TextView name;
        @BindView(R.id.white_code)
        TextView white_code;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    private void setAnimation(View viewToAnimate, int position) {
        if (position > lastPosition) {
            Animation animation = AnimationUtils.loadAnimation(mActivity, android.R.anim.slide_in_left);
            animation.setDuration(1000);
            viewToAnimate.startAnimation(animation);
            lastPosition = position;
        } else if ( position < lastPosition) {
            Animation animation = AnimationUtils.loadAnimation(mActivity, android.R.anim.slide_in_left);
            viewToAnimate.startAnimation(animation);
            lastPosition = position;
        }
    }

}
