package model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.util.ArrayList;

public class DriverListData {

    @SerializedName("record")
    @Expose
    private ArrayList<Record> record = null;

    public ArrayList<Record> getRecord() {
        return record;
    }

    public void setRecord(ArrayList<Record> record) {
        this.record = record;
    }
}
