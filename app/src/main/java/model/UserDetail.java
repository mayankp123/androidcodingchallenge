package model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UserDetail {

    @SerializedName("customer_id")
    @Expose
    private String customerId;
    @SerializedName("firstname")
    @Expose
    private String firstname;
    @SerializedName("lastname")
    @Expose
    private String lastname;
    @SerializedName("mobileno")
    @Expose
    private String mobileno;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("password")
    @Expose
    private Object password;
    @SerializedName("profilepic_url")
    @Expose
    private String profilepicUrl;
    @SerializedName("devicetype")
    @Expose
    private Integer devicetype;
    @SerializedName("country_code")
    @Expose
    private Integer countryCode;
    @SerializedName("device_token")
    @Expose
    private Object deviceToken;
    @SerializedName("otp")
    @Expose
    private Object otp;
    @SerializedName("language_code")
    @Expose
    private String languageCode;
    @SerializedName("whideCode")
    @Expose
    private String whideCode;
    @SerializedName("bolt")
    @Expose
    private Double bolt;
    @SerializedName("socket_token")
    @Expose
    private String socketToken;
    @SerializedName("token")
    @Expose
    private String token;
    @SerializedName("qrCodeUrl")
    @Expose
    private String qrCodeUrl;
    @SerializedName("inviteCode")
    @Expose
    private Object inviteCode;
    @SerializedName("brand")
    @Expose
    private Object brand;
    @SerializedName("model")
    @Expose
    private Object model;
    @SerializedName("osVersion")
    @Expose
    private Object osVersion;
    @SerializedName("gender")
    @Expose
    private Object gender;
    @SerializedName("buildVersion")
    @Expose
    private Object buildVersion;

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getMobileno() {
        return mobileno;
    }

    public void setMobileno(String mobileno) {
        this.mobileno = mobileno;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Object getPassword() {
        return password;
    }

    public void setPassword(Object password) {
        this.password = password;
    }

    public String getProfilepicUrl() {
        return profilepicUrl;
    }

    public void setProfilepicUrl(String profilepicUrl) {
        this.profilepicUrl = profilepicUrl;
    }

    public Integer getDevicetype() {
        return devicetype;
    }

    public void setDevicetype(Integer devicetype) {
        this.devicetype = devicetype;
    }

    public Integer getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(Integer countryCode) {
        this.countryCode = countryCode;
    }

    public Object getDeviceToken() {
        return deviceToken;
    }

    public void setDeviceToken(Object deviceToken) {
        this.deviceToken = deviceToken;
    }

    public Object getOtp() {
        return otp;
    }

    public void setOtp(Object otp) {
        this.otp = otp;
    }

    public String getLanguageCode() {
        return languageCode;
    }

    public void setLanguageCode(String languageCode) {
        this.languageCode = languageCode;
    }

    public String getWhideCode() {
        return whideCode;
    }

    public void setWhideCode(String whideCode) {
        this.whideCode = whideCode;
    }

    public Double getBolt() {
        return bolt;
    }

    public void setBolt(Double bolt) {
        this.bolt = bolt;
    }

    public String getSocketToken() {
        return socketToken;
    }

    public void setSocketToken(String socketToken) {
        this.socketToken = socketToken;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getQrCodeUrl() {
        return qrCodeUrl;
    }

    public void setQrCodeUrl(String qrCodeUrl) {
        this.qrCodeUrl = qrCodeUrl;
    }

    public Object getInviteCode() {
        return inviteCode;
    }

    public void setInviteCode(Object inviteCode) {
        this.inviteCode = inviteCode;
    }

    public Object getBrand() {
        return brand;
    }

    public void setBrand(Object brand) {
        this.brand = brand;
    }

    public Object getModel() {
        return model;
    }

    public void setModel(Object model) {
        this.model = model;
    }

    public Object getOsVersion() {
        return osVersion;
    }

    public void setOsVersion(Object osVersion) {
        this.osVersion = osVersion;
    }

    public Object getGender() {
        return gender;
    }

    public void setGender(Object gender) {
        this.gender = gender;
    }

    public Object getBuildVersion() {
        return buildVersion;
    }

    public void setBuildVersion(Object buildVersion) {
        this.buildVersion = buildVersion;
    }

}