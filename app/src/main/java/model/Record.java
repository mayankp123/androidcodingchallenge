package model;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Entity
public class Record {

    @PrimaryKey(autoGenerate = true)
    @SerializedName("driver_id")
    @Expose
    private Long driverId;
    @ColumnInfo(name = "userId")
    @SerializedName("userId")
    @Expose
    private Long userId;
    @ColumnInfo(name = "name")
    @SerializedName("name")
    @Expose
    private String name;
    @ColumnInfo(name = "mobileNo")
    @SerializedName("mobileNo")
    @Expose
    private String mobileNo;
    @ColumnInfo(name = "imageUrl")
    @SerializedName("imageUrl")
    @Expose
    private String imageUrl;
    @ColumnInfo(name = "whideTrust")
    @SerializedName("whideTrust")
    @Expose
    private Double whideTrust;
    @ColumnInfo(name = "perHourPrice")
    @SerializedName("perHourPrice")
    @Expose
    private Integer perHourPrice;
    @ColumnInfo(name = "whideCode")
    @SerializedName("whideCode")
    @Expose
    private String whideCode;

    public Long getDriverId() {
        return driverId;
    }

    public void setDriverId(Long driverId) {
        this.driverId = driverId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public Double getWhideTrust() {
        return whideTrust;
    }

    public void setWhideTrust(Double whideTrust) {
        this.whideTrust = whideTrust;
    }

    public Integer getPerHourPrice() {
        return perHourPrice;
    }

    public void setPerHourPrice(Integer perHourPrice) {
        this.perHourPrice = perHourPrice;
    }

    public String getWhideCode() {
        return whideCode;
    }

    public void setWhideCode(String whideCode) {
        this.whideCode = whideCode;
    }

}
