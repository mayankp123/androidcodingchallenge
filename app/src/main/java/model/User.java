package model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class User {

    @SerializedName("userId")
    @Expose
    public String userId;
    @SerializedName("password")
    @Expose
    public String password;

    public User(String userId, String password) {
        this.userId = userId;
        this.password = password;
    }

}
