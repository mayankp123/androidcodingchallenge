package com.example.androidcodingchallenge;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;
import static com.example.androidcodingchallenge.MainActivity.MY_PREFERENCES;
import static com.example.androidcodingchallenge.MainActivity.TOKEN;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.widget.ProgressBar;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import java.util.ArrayList;
import java.util.List;
import adapter.DriverListAdapter;
import butterknife.BindView;
import butterknife.ButterKnife;
import localdb.DatabaseClient;
import model.DriverListData;
import model.Record;
import network.RetrofitClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SecondActivity extends AppCompatActivity {

    public static final String TAG = "SecondActivity";

    public static final String DRIVER_DATA_STORED = "driver_data_stored";

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.progress_bar)
    ProgressBar progressBar;

    private ArrayList<Record> driverList;
    private DriverListAdapter driverListAdapter;
    GridLayoutManager gridLayoutManager;

    private boolean isDriverDataStored = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.second_activity);
        ButterKnife.bind(this);

        isDriverDataStored();
    }

    private void isDriverDataStored() {
        SharedPreferences sp = getSharedPreferences(MY_PREFERENCES ,Context.MODE_PRIVATE);
        isDriverDataStored  = sp.getBoolean(DRIVER_DATA_STORED,false);

        if(isDriverDataStored) {
            getDataFromDb();
        }else {
            getDriverListFromApi();
        }
    }

    private void getDriverListFromApi() {
        driverList = new ArrayList<>();
        SharedPreferences sp = getSharedPreferences(MY_PREFERENCES , Context.MODE_PRIVATE);

        progressBar.setVisibility(VISIBLE);
        Call<DriverListData> call = RetrofitClient.getInstance().getMyApi().getDriverList(sp.getString(TOKEN, "null"));
        call.enqueue(new Callback<DriverListData>() {
            @Override
            public void onResponse(Call<DriverListData> call, Response<DriverListData> response) {
                if(response.isSuccessful()) {
                    progressBar.setVisibility(GONE);
                    Log.d(TAG, response.body().toString());
                    driverList.addAll(response.body().getRecord());

                    //saving to database
                    saveDataInDb(driverList);

                    SharedPreferences sp = getSharedPreferences(MY_PREFERENCES, Context.MODE_PRIVATE);
                    sp.edit().putBoolean(DRIVER_DATA_STORED, true).commit();

                    driverListAdapter = new DriverListAdapter(SecondActivity.this, driverList);
                    gridLayoutManager = new GridLayoutManager(SecondActivity.this, 2, GridLayoutManager.VERTICAL, false);
                    recyclerView.setLayoutManager(gridLayoutManager);
                    recyclerView.setAdapter(driverListAdapter);
                    driverListAdapter.notifyDataSetChanged();
                }else{
                    progressBar.setVisibility(GONE);
                    Log.d(TAG, response.errorBody().toString());
                    Toast.makeText(SecondActivity.this, response.errorBody().toString(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<DriverListData> call, Throwable t) {
                progressBar.setVisibility(GONE);
                Log.d(TAG, t.getMessage());
                Toast.makeText(SecondActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void saveDataInDb(ArrayList<Record> driverList) {
        class SaveTask extends AsyncTask<Void, Void, Void> {

            @Override
            protected Void doInBackground(Void... voids) {

                //adding to database
                DatabaseClient.getInstance(getApplicationContext()).getAppDatabase()
                        .taskDao()
                        .insert(driverList);
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                Toast.makeText(getApplicationContext(), "All data saved in Database", Toast.LENGTH_LONG).show();
            }
        }
        SaveTask st = new SaveTask();
        st.execute();
    }

    private void getDataFromDb() {
        class GetTasks extends AsyncTask<Void, Void, List<Record>> {
            @Override
            protected List<Record> doInBackground(Void... voids) {
                List<Record> taskList = DatabaseClient
                        .getInstance(getApplicationContext())
                        .getAppDatabase()
                        .taskDao()
                        .getAll();
                return taskList;
            }

            @Override
            protected void onPostExecute(List<Record> tasks) {
                super.onPostExecute(tasks);
                driverListAdapter = new DriverListAdapter(SecondActivity.this, tasks);
                gridLayoutManager = new GridLayoutManager(SecondActivity.this, 2, GridLayoutManager.VERTICAL, false);
                recyclerView.setLayoutManager(gridLayoutManager);
                recyclerView.setAdapter(driverListAdapter);
                driverListAdapter.notifyDataSetChanged();
            }
        }

        GetTasks gt = new GetTasks();
        gt.execute();
    }

}
