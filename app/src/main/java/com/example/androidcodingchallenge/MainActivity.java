package com.example.androidcodingchallenge;

import static android.view.View.VISIBLE;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import java.util.ArrayList;
import java.util.List;
import butterknife.BindView;
import butterknife.ButterKnife;
import model.LoginData;
import model.User;
import network.RetrofitClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    public static final String TAG = "MainActivity";
    private static final int PERMISSION_REQUEST_CODE = 100;

    public static final String MY_PREFERENCES = "MyPrefs" ;
    public static final String LOGIN_STATUS = "status";
    public static final String TOKEN = "token";

    private boolean loginStatus = false;

    @BindView(R.id.login_text)
    TextView loginText;
    @BindView(R.id.progress_bar)
    ProgressBar progressBar;

    String[] permissions = new String[]{
            Manifest.permission.INTERNET,
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        if(checkPermissions()) {
            isUserLoggedIn();
            setListeners();
        }
    }

    private void isUserLoggedIn() {
        SharedPreferences sp = getSharedPreferences(MY_PREFERENCES ,Context.MODE_PRIVATE);
        loginStatus  = sp.getBoolean(LOGIN_STATUS,false);

        if(loginStatus) {
            // if user is already logged in, then move to next activity directly without login again...
            Intent intent = new Intent(MainActivity.this, SecondActivity.class);
            startActivity(intent);
            finish();
        } else {
            // perform login operation...
            loginCall();
        }
    }

    private void setListeners() {
        loginText.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.login_text:
                loginCall();
                break;
        }
    }

    private void loginCall() {
       // perform login api call...
        progressBar.setVisibility(VISIBLE);
        Call<LoginData> call = RetrofitClient.getInstance().getMyApi().loginCall(new User("9041422652", "123456"));
        call.enqueue(new Callback<LoginData>() {
            @Override
            public void onResponse(Call<LoginData> call, Response<LoginData> response) {
                if(response.isSuccessful()) {
                    progressBar.setVisibility(View.GONE);
                    Log.d(TAG, response.body().toString());
                    // Save login status 'true' in shared pref, if api response is successful...
                    SharedPreferences sp = getSharedPreferences(MY_PREFERENCES, Context.MODE_PRIVATE);
                    sp.edit().putBoolean(LOGIN_STATUS, true).commit();
                    sp.edit().putString(TOKEN, response.body().getUserDetail().getToken()).commit();

                    // move to next activity...
                    Intent intent = new Intent(MainActivity.this, SecondActivity.class);
                    startActivity(intent);
                    finish();
                }else {
                    progressBar.setVisibility(View.GONE);
                    Log.d(TAG, response.errorBody().toString());
                    Toast.makeText(MainActivity.this, response.errorBody().toString(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<LoginData> call, Throwable t) {
                Log.d(TAG, t.getMessage());
                progressBar.setVisibility(View.GONE);
                Toast.makeText(MainActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private boolean checkPermissions() {
        int result;
        List<String> listPermissionsNeeded = new ArrayList<>();
        for (String p : permissions) {
            result = ContextCompat.checkSelfPermission(this, p);
            if (result != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(p);
            }
        }
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(this, listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), PERMISSION_REQUEST_CODE);
            return false;
        }
        return true;
    }

    @SuppressLint("MissingSuperCall")
    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        if (requestCode == PERMISSION_REQUEST_CODE) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // do something
                setListeners();
                Toast.makeText(MainActivity.this, "Permission Granted!", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(MainActivity.this, "Permission Denied! Please allow this permission in App Settings.", Toast.LENGTH_SHORT).show();
            }
            return;
        }
    }
}